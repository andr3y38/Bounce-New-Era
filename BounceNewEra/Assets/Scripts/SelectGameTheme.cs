using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectGameTheme : MonoBehaviour
{
    public Button[] ThemeButtons;
    public Button Next;
    public GameObject Panel;

    private int indexTheme = -1;

    private void Start()
    {
        for (int i = 0; i < ThemeButtons.Length; i++)
        {
            int index = i;
            ThemeButtons[i].onClick.AddListener(() => OnClickThemeButton(index));
        }
        Next.onClick.AddListener(OnClickNextButton);
    }

    private void OnClickThemeButton(int index)
    {
        indexTheme = index;
    }
    private void OnClickNextButton()
    {
        if(indexTheme == 0)
        {
            Loading.Load(Loading.Scene.menu);
        }
        else
        {
            Panel.SetActive(true);
        }
    }
}
