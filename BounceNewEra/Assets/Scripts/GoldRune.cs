using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldRune : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            PlayerPrefs.SetInt("GoldRunes", +1);
            Destroy(gameObject);
        }
    }
}
