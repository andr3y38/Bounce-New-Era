using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckSkinWasPurchased : MonoBehaviour
{
    public GameObject[] NormalSkins;
    public GameObject[] Cosmetics;
    private ClassicBallsShopManager _classicBallsShopRef;
    private CosmeticShopManager _cosmeticShopManagerRef;

    void Awake()
    {
        _classicBallsShopRef = FindObjectOfType<ClassicBallsShopManager>();
        _cosmeticShopManagerRef = FindObjectOfType<CosmeticShopManager>();
    }

    void Start()
    {
        foreach (GameObject skin in NormalSkins)
        {
            skin.SetActive(false);
        }
        foreach (GameObject cosmetic in Cosmetics)
        {
            cosmetic.SetActive(false);
        }
        CheckIFWasPurchased();
    }
    
    public void CheckIFWasPurchased()
    {
        foreach (ShopDetails ball in _classicBallsShopRef.balls)
        {
            if (ball.isUnlocked)
            {
                NormalSkins[ball.index].SetActive(true);
            }
        }
        
        foreach (ShopDetails cosmetic in _cosmeticShopManagerRef.Cosmetics)
        {
            if (cosmetic.isUnlocked)
            {
                Cosmetics[cosmetic.index].SetActive(true);
            }
        }
    }
}
