using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SetShopIndex : MonoBehaviour
{ 
    public int SetIndex;
    
    [Header("Classic Ball")]
    private ClassicBallsShopManager _classicBallsShopRef;
    private CharacterSetup characterSetupRef;
    
    [Header("Cosmetics")]
    private CosmeticShopManager _cosmeticShopRef;
    // TO DO!! inventory setup for cosmetics
    
    private void Awake()
    {
        _classicBallsShopRef = FindObjectOfType<ClassicBallsShopManager>();
        characterSetupRef = FindObjectOfType<CharacterSetup>();
        _cosmeticShopRef = FindObjectOfType<CosmeticShopManager>();
    }

    public void SetIndexCosmetics()
    {
        _cosmeticShopRef.CosmeticSkins[_cosmeticShopRef.currentCosmeticIndex].SetActive(false);
        _cosmeticShopRef.currentCosmeticIndex = SetIndex;
        _cosmeticShopRef.CosmeticSkins[_cosmeticShopRef.currentCosmeticIndex].SetActive(true);
        _cosmeticShopRef.UpdateUI();
    }
    
   public void SetIndexClassicBalls()
    {
        _classicBallsShopRef.ballSkins[_classicBallsShopRef.currentBallIndex].SetActive(false);
        _classicBallsShopRef.currentBallIndex = SetIndex;
        _classicBallsShopRef.ballSkins[_classicBallsShopRef.currentBallIndex].SetActive(true);
        _classicBallsShopRef.UpdateUI();
    }

   public void SetCharacterIndexClassicBalls()
   {
       characterSetupRef.Characters[characterSetupRef.ballIndex].SetActive(false);
       characterSetupRef.ballIndex = SetIndex;
       PlayerPrefs.SetInt("BallIndex", characterSetupRef.ballIndex);
       characterSetupRef.Characters[characterSetupRef.ballIndex].SetActive(true);
   }
   
   //to do Set
   
}
