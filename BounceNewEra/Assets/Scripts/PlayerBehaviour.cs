using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using FMODUnity;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera RightView;
    private FMOD.Studio.EventInstance Movement;
    public int NumberJumps = 0;     // Current number of jumps
    private Rigidbody rb;           // Access rigidbody
    private bool AllowedToJump;      // Allow to jump variable bool
    private float distToGround = 1f;
    private StudioEventEmitter _emitter;
    private Vector2 startTouchPosition;
    private Vector2 endTouchPosition;
    //private Animator anim;                  // Store Animation Sequence
    
    public Button button;
    public float jumpForce;         // Jump force speed
    public float rollSpeed;         // Movement rotation speed
    public int MaxJumps;            // Maximum number of jumps
    public float minRPM = 0;
    public float maxRPM = 10000;
    public VariableJoystick variableJoystick;
    public int Health = 100;
    
    
    /*public float magnetRadius = 10.0f; // The radius within which the runes will be attracted
    public float magnetForce = 5.0f; // The strength of the attraction
    private GameObject[] runes; // An array to store all the runes in the scene*/

    // Start is called before the first frame update
    void Start()
    {
        _emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        rb = GetComponent<Rigidbody>();
        button.onClick.AddListener(Jump); 
        // anim = gameObject.GetComponent<Animator>();
        // runes = GameObject.FindGameObjectsWithTag("Rune");
    }
    

    void Update()
    {
        // Grabbing the actual speed of the ball
        float vel = rb.velocity.magnitude;
        
        // Interpolating between the minimum and maximum value of my FMOD Parameter using the "normalised" speed
        float effectiveRPM = Mathf.Lerp(minRPM, maxRPM, vel / 18.0f);

        //Sound rolling
        if (Physics.Raycast(transform.position, Vector3.down, distToGround + 0.1f))
        {
            // Sending the speed value to that emitter
            _emitter.SetParameter("Air", 0);
            _emitter.SetParameter("Speed", effectiveRPM);
        }
        else
        {
            _emitter.SetParameter("Air", 1);
        }
        
        //Slide to jump
        SlideToJump();
    }

    
    void FixedUpdate()
    {
        Vector3 forward = Camera.main.transform.forward;
        Vector3 right = Camera.main.transform.right;
        forward.y = 0;
        right.y = 0;
        forward = forward.normalized;
        right = right.normalized;

        Vector3 ForwardDirection = forward * (variableJoystick.Vertical * rollSpeed);
        Vector3 RightDirection = right * (variableJoystick.Horizontal * rollSpeed);
        Vector3 Move = ForwardDirection + RightDirection;
        Vector3 jumpgravity = new Vector3(0, rb.velocity.y, 0);
        rb.velocity = Move + jumpgravity;

        if (!Physics.Raycast(transform.position, Vector3.down, distToGround + 0.2f))
        {
            AllowedToJump = false;
        }

        if (AllowedToJump && NumberJumps <= MaxJumps)
        {
            // anim.SetTrigger("Trigger");
            rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
        }
    }

   /* private void RunesMagnet()
    {
        foreach (GameObject rune in runes)
        {
            if(rune != null)
            {
                float distance;
                distance = Vector3.Distance(transform.position, rune.transform.position);

                if (distance < magnetRadius)
                {
                    // Calculate the direction of the attraction
                    Vector3 attractionDirection = (transform.position - rune.transform.position).normalized;

                    // Interpolate the position of the rune towards the ball
                    rune.transform.position = Vector3.Lerp(rune.transform.position, transform.position, magnetForce * Time.deltaTime);
                }
            }
        }
    }*/
    
    
    public void Jump()
    {
        if (NumberJumps < MaxJumps && Physics.Raycast(transform.position, Vector3.down, distToGround + 0.1f))
        {
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z); // Reset the vertical velocity before jumping
            rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange); // Apply an immediate vertical force
            NumberJumps++;
        }
    }

    private void SlideToJump()
    {
        
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchPosition = Input.GetTouch(0).position;
        }

        else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouchPosition = Input.GetTouch(0).position;
            if (endTouchPosition.y > startTouchPosition.y && endTouchPosition.x > Screen.width / 2)
            {
                Jump();
            }
        }
    }

    private void ActivateShield()
    {
        Health += 100;
        //Particle

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Meteorite"))
        {
            Health -= 100;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("RightView"))
        {
            RightView.Priority = 4;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("RightView"))
        {
            RightView.Priority = 1;
        }
    }
}




