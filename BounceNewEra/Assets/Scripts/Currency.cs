using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Currency : MonoBehaviour
{
    public TMP_Text CommonInventory;
    public TMP_Text GoldInventory;

  
    // Update is called once per frame
    void Update()
    {
        CommonInventory.text = "" + PlayerPrefs.GetInt("NormalRunes");
        GoldInventory.text = "" + PlayerPrefs.GetInt("GoldRunes");
    }
}
