using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rune : MonoBehaviour
{
    public GameObject ParentDestroy;
    public float magnetForce = 5.0f;
    [SerializeField] string EventName;

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            PlayerPrefs.SetInt("NormalRunes", +1);
            OpenPortal.CurrentNumberOfRunes += 1;
            ParentDestroy.GetComponent<Animator>().Play(EventName);
            Destroy(ParentDestroy, 1.75f);
        }
    }
    private void Update()
    {
        transform.Rotate(Vector3.forward, 45 * Time.deltaTime);
    }

}
