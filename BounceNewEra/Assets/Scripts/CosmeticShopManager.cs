using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class CosmeticShopManager : MonoBehaviour
{
    public int currentCosmeticIndex = 0;
    public GameObject[] CosmeticSkins;
    public ShopDetails[] Cosmetics;
    public Button BuyButton;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (ShopDetails Cosmetic in Cosmetics)
        {
            if (Cosmetic.price == 0)
                Cosmetic.isUnlocked = true;
            else
                Cosmetic.isUnlocked = PlayerPrefs.GetInt(Cosmetic.name, 0) == 0 ? false : true;
        }
        foreach (GameObject ball in CosmeticSkins)
        {
            ball.SetActive(false);
        }
    }

    public void Buy()
    {
        ShopDetails shop = Cosmetics[currentCosmeticIndex];
        PlayerPrefs.SetInt(shop.name, 1);
        PlayerPrefs.SetInt("Cosmetic", currentCosmeticIndex);
        shop.isUnlocked = true;
        PlayerPrefs.SetInt("NormalRunes", PlayerPrefs.GetInt("NormalRunes", 0) - shop.price);
        UpdateUI();
    }  
  
    public void UpdateUI()
    {
        ShopDetails shop = Cosmetics[currentCosmeticIndex];
        if (shop.isUnlocked)
        {
            BuyButton.gameObject.SetActive(false);
        }
        else
        {
            BuyButton.gameObject.SetActive(true);
            BuyButton.GetComponentInChildren<TMP_Text>().text = "" + shop.price;
            if (shop.price <= PlayerPrefs.GetInt("NormalRunes", 0))
            {
                BuyButton.interactable = true;
            }
            else
            {
                BuyButton.interactable = false;
            }
        }
    }
}