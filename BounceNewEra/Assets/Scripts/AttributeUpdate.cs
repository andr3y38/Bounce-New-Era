using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AttributeUpdate : MonoBehaviour
{
    
    public TMP_Text intValueInput; // Reference to the integer input field
    public Slider slider; // Reference to the slider

    // Start is called before the first frame update
    void Start()
    {
        UpdateSliderValue();
    }

    public void UpdateSliderValue()
    {
        if (intValueInput != null && slider != null)
        {
            int value;
            if (int.TryParse(intValueInput.text, out value))
            {
                // Ensure the value is within the slider's min and max range
                value = Mathf.Clamp(value, (int)slider.minValue, (int)slider.maxValue);
                slider.value = value;
            }
        }
    }
  
}
