using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OpenPortal : MonoBehaviour
{
    public TMP_Text CollectedRunes;
    public int NumberOfMaximumRunes;
    public static int CurrentNumberOfRunes;

    // Start is called before the first frame update
    void Start()
    {
        CurrentNumberOfRunes = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CollectedRunes.GetComponent<TMP_Text>().text = "" + CurrentNumberOfRunes;
        if (CurrentNumberOfRunes == NumberOfMaximumRunes)
        {
            GetComponent<Animator>().Play("Portal");
        }    
    }
}
