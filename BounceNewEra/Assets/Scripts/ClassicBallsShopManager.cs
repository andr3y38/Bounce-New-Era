using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class ClassicBallsShopManager : MonoBehaviour
{
    public int currentBallIndex = 0;
    public GameObject[] ballSkins;
    public ShopDetails[] balls;
    public Button BuyButton;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (ShopDetails ball in balls)
        {
            if (ball.price == 0)
                ball.isUnlocked = true;
            else
                ball.isUnlocked = PlayerPrefs.GetInt(ball.name, 0) == 0 ? false : true;
        }
        foreach (GameObject ball in ballSkins)
        {
            ball.SetActive(false);
        }
    }

    public void Buy()
    {
        ShopDetails b = balls[currentBallIndex];
        PlayerPrefs.SetInt(b.name, 1);
        PlayerPrefs.SetInt("Ball", currentBallIndex);
        b.isUnlocked = true;
        PlayerPrefs.SetInt("NormalRunes", PlayerPrefs.GetInt("NormalRunes", 0) - b.price);
        UpdateUI();
    }  
  
    public void UpdateUI()
    {
        ShopDetails b = balls[currentBallIndex];
        if (b.isUnlocked)
        {
            BuyButton.gameObject.SetActive(false);
        }
        else
        {
            BuyButton.gameObject.SetActive(true);
            BuyButton.GetComponentInChildren<TMP_Text>().text = "" + b.price;
            if (b.price <= PlayerPrefs.GetInt("NormalRunes", 0))
            {
                BuyButton.interactable = true;
            }
            else
            {
                BuyButton.interactable = false;
            }
        }
    }
}
