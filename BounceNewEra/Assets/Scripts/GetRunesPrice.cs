using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class GetRunesPrice : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI RuneText;
    public Button BuyButton;

    // Start is called before the first frame update
    void Start()
    {
        UpdateRuneText(); // Update the rune text initially
    }

    // Update the rune text when needed
    public void UpdateRuneText()
    {
        if (BuyButton != null)
        {
            TextMeshProUGUI buttonText = BuyButton.GetComponentInChildren<TextMeshProUGUI>();
            if (buttonText != null)
            {
                RuneText.text = buttonText.text;
            }
        }
    }
}