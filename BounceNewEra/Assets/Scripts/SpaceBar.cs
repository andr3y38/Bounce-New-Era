using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceBar : MonoBehaviour
{
    public Button canvasButton;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            canvasButton.onClick.Invoke();
        }
    }
}
