using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSetup : MonoBehaviour
{
    public int ballIndex;
    public GameObject[] Characters;
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject caracter in Characters)
        {
            caracter.SetActive(false);
        }
        ballIndex = PlayerPrefs.GetInt("BallIndex", 0);
        Characters[ballIndex].SetActive(true);
    }
 
}
