using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class UseSkin : MonoBehaviour
{
    [SerializeField]
    private GameObject[] skins;

    [SerializeField]
    private GameObject[] skinToEquip;
    
    [SerializeField]
    private GameObject[] menuShowActualSkin;

    [SerializeField]
    private GameObject[] menuReadySkin;
    
    private int activeSkinIndex = -1;
    
    private void Start()
    {
        LoadActiveSkin();
    }

    public void SetSkin()
    {
        // Reset the active state of all menuShowActualSkin GameObjects
        foreach (GameObject menuSkin in menuShowActualSkin)
        {
            menuSkin.SetActive(false);
        }
     foreach (GameObject ReadySkin in menuReadySkin)
        {
            ReadySkin.SetActive(false);
        }
    
        for (int i = 0; i < skins.Length; i++)
        {
            if (skins.Length > i && skins[i].activeSelf)
            {
                activeSkinIndex = i;
                break;
            }
        }
        SaveSkin();
        SaveSkinForRelog();
    }

    private void SaveSkin()
    {
        if (activeSkinIndex != -1)
        {
            SkinLoader.skinToLoad = skinToEquip[activeSkinIndex];
            Debug.Log("Working Set NO: " + activeSkinIndex);
            menuShowActualSkin[activeSkinIndex].SetActive(true);
            menuReadySkin[activeSkinIndex].SetActive(true);
        }
        else
        {
            // No skin is active, set default skin
            activeSkinIndex = 0;
            SkinLoader.skinToLoad = skinToEquip[0];
            Debug.Log("Default skin is active.");
            menuShowActualSkin[0].SetActive(true);
            menuReadySkin[0].SetActive(true);
        }
    }
    
    private void SaveSkinForRelog()
    {
        PlayerPrefs.SetInt("ActiveSkinIndex", activeSkinIndex); 
        Debug.Log("Saved" + activeSkinIndex);
    }

    private void LoadActiveSkin()
    {
        activeSkinIndex = PlayerPrefs.GetInt("ActiveSkinIndex", -1);
        Debug.Log("Working Wake Up NO: " + activeSkinIndex);

        if (activeSkinIndex == -1)
        {
            // Set default ball as the active skin and enable the corresponding menu item
            SkinLoader.skinToLoad = skinToEquip[0];
            Debug.Log("Default ball is active.");
            menuShowActualSkin[0].SetActive(true);
            menuReadySkin[0].SetActive(true);
        }
        
        else if (activeSkinIndex != -1 && activeSkinIndex < skinToEquip.Length)
        {
            SkinLoader.skinToLoad = skinToEquip[activeSkinIndex];
            Debug.Log("Working NO: " + activeSkinIndex);
            menuShowActualSkin[activeSkinIndex].SetActive(true);
            menuReadySkin[activeSkinIndex].SetActive(true);
        }
        
    }
}
