using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accessories : MonoBehaviour
{
    public GameObject ball;
    public float yOffset = 1f;

    private Transform ballTransform;
    private Transform hatTransform;

    void Start()
    {
        ballTransform = ball.transform;
        hatTransform = transform;
    }

    void LateUpdate()
    {
        // Update the position of the hat above the ball
        hatTransform.position = ballTransform.position + new Vector3(0f, yOffset, 0f);
        
        // Keep the hat's rotation fixed
        hatTransform.rotation = Quaternion.identity;
    }
}
