using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinLoader : MonoBehaviour
{
    public GameObject defaultSkin;
    public static GameObject skinToLoad;


    void Awake()
    {
        if (skinToLoad != null)
        {
            Destroy(defaultSkin);
            Instantiate(skinToLoad, transform);
        }
    }

    
}
