using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public Dropdown fpsDropdown;
    private const string FPSKey = "TargetFPS";
    public ToggleGroup qualityToggleGroup;
    private const string QualityKey = "SelectedQuality";

    private void Start()
    {
        LoadFPS(); // Load FPS setting from PlayerPrefs when the game starts
        LoadQualityLevel();
    }

    public void SetFPS(int value)
    {
        int targetFrameRate = value == 0 ? 60 : 30; // Set target frame rate based on the selected value
        Application.targetFrameRate = targetFrameRate;
        SaveFPS(targetFrameRate); // Save the selected FPS setting to PlayerPrefs
    }

    private void LoadFPS()
    {
        int savedFPS = PlayerPrefs.GetInt(FPSKey, 60); // Load saved FPS setting (default to 60 if not found)
        fpsDropdown.value = savedFPS == 60 ? 0 : 1; // Set dropdown value based on loaded FPS
        Application.targetFrameRate = savedFPS;
    }

    private void SaveFPS(int value)
    {
        PlayerPrefs.SetInt(FPSKey, value); // Save the selected FPS setting to PlayerPrefs
    }

    public void SetQuality(string qualityName)
    {
        QualitySettings.SetQualityLevel(QualitySettings.names.ToList().IndexOf(qualityName));
        SaveQualityLevel(qualityName);
    }
    
    private void LoadQualityLevel()
    {
        string savedQualityName = PlayerPrefs.GetString(QualityKey, "Low");

        Toggle[] toggles = qualityToggleGroup.GetComponentsInChildren<Toggle>();
        foreach (Toggle toggle in toggles)
        {
            if (toggle.name == savedQualityName)
            {
                toggle.isOn = true;
            }
            else
            {
                toggle.isOn = false;
            }
        }

        QualitySettings.SetQualityLevel(QualitySettings.names.ToList().IndexOf(savedQualityName));
    }

    private void SaveQualityLevel(string qualityName)
    {
        PlayerPrefs.SetString(QualityKey, qualityName);
    }
}
