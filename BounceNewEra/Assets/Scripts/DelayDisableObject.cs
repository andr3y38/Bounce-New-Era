using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayDisableObject : MonoBehaviour
{
    public float delay = 0.5f;

    public void Delay()
    {
        StartCoroutine(DisableObjectCoroutine());
    }

    private IEnumerator DisableObjectCoroutine()
    {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);  // Disable the GameObject where the script is attached
    }
}
