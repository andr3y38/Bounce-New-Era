using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagers : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene(2);
    }
    
    public void Theme()
    {
        SceneManager.LoadScene(0);
    }
    public void MenuScene()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OnQuit()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
