using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ShopManagerGold : MonoBehaviour
{
    public int currentBallIndex;
    public GameObject[] ballSkins;
    public ShopDetails[] balls;
    public Button BuyButtonSpecial;
    public Button SelectButton1;
    public Button SelectButton2;

    // Start is called before the first frame update
    void Start()
    {
        foreach (ShopDetails ball in balls)
        {
            if (ball.price == 0)
                ball.isUnlocked = true;
            else
                ball.isUnlocked = PlayerPrefs.GetInt(ball.name, 0) == 0 ? false : true;
        }

        currentBallIndex = PlayerPrefs.GetInt("GoldBall", 0);
        foreach (GameObject ball in ballSkins)
            ball.SetActive(false);

        ballSkins[currentBallIndex].SetActive(true);

       
    }
    public void Next()
    {
        ballSkins[currentBallIndex].SetActive(false);

        currentBallIndex++;
        if (currentBallIndex == ballSkins.Length)
            currentBallIndex = 0;

        ballSkins[currentBallIndex].SetActive(true);
        ShopDetails b = balls[currentBallIndex];
        if (!b.isUnlocked)
            return;


        PlayerPrefs.SetInt("GoldBall", currentBallIndex);

    }

    public void Back()
    {
        ballSkins[currentBallIndex].SetActive(false);

        currentBallIndex--;
        if (currentBallIndex == -1)
            currentBallIndex = ballSkins.Length -1;

        ballSkins[currentBallIndex].SetActive(true);

        ShopDetails b = balls[currentBallIndex];
        if (!b.isUnlocked)
            return; 

        PlayerPrefs.GetInt("Ball", currentBallIndex);

    }
    private void Update()
    {
        UpdateUIGOLD();
    }

    public void BuySpecial()
    {
        ShopDetails b = balls[currentBallIndex];

        PlayerPrefs.SetInt(b.name, 1);
        PlayerPrefs.SetInt("Ball", currentBallIndex);
        b.isUnlocked = true;
        PlayerPrefs.SetInt("GoldRunes", PlayerPrefs.GetInt("GoldRunes", 0) - b.price);
    }
   
    private void UpdateUIGOLD()
    {
        ShopDetails b = balls[currentBallIndex];
        if (b.isUnlocked)
        {
            BuyButtonSpecial.gameObject.SetActive(false);
            SelectButton1.interactable = true;
            SelectButton2.interactable = true;
        }
        else
        {
            BuyButtonSpecial.gameObject.SetActive(true);
            SelectButton1.interactable = false;
            SelectButton2.interactable = false;

            BuyButtonSpecial.GetComponentInChildren<TMP_Text>().text = "Buy special for " + b.price;
            if (b.price <= PlayerPrefs.GetInt("GoldRunes", 0))
            {
                BuyButtonSpecial.interactable = true;
            }
            else
            {
                BuyButtonSpecial.interactable = false;
            }
        }
    }
}
