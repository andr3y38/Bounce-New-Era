L  �u
                         LOD_FADE_CROSSFADE     _ADDITIONAL_LIGHTS     _MAIN_LIGHT_SHADOWS_CASCADE    _MIXED_LIGHTING_SUBTRACTIVE A  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _TimeParameters;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_RenderingLayer;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM mediump vec4 unity_SpecCube1_HDR;
	UNITY_UNIFORM vec4 unity_SpecCube0_BoxMax;
	UNITY_UNIFORM vec4 unity_SpecCube0_BoxMin;
	UNITY_UNIFORM vec4 unity_SpecCube0_ProbePosition;
	UNITY_UNIFORM vec4 unity_SpecCube1_BoxMax;
	UNITY_UNIFORM vec4 unity_SpecCube1_BoxMin;
	UNITY_UNIFORM vec4 unity_SpecCube1_ProbePosition;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixPreviousM[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixPreviousMI[4];
	UNITY_UNIFORM vec4 unity_MotionVectorsParams;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _TextureSample_ST;
	UNITY_UNIFORM float _WindJitter;
	UNITY_UNIFORM float _WindScroll;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(4) uniform mediump sampler2D _WindNoiseTexture;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_COLOR0;
in highp vec4 in_TEXCOORD0;
out highp vec3 vs_TEXCOORD0;
out highp float vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD4;
out highp vec4 vs_TEXCOORD5;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xzxz;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0].xzxz * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2].xzxz * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3].xzxz * in_POSITION0.wwww + u_xlat0;
    u_xlat0 = u_xlat0 * vec4(0.100000001, 0.100000001, 0.200000003, 0.200000003);
    u_xlat1 = _TimeParameters.xxxx * vec4(_WindScroll, _WindScroll, _WindJitter, _WindJitter);
    u_xlat0 = u_xlat1 * vec4(0.300000012, 0.300000012, 0.5, 0.5) + u_xlat0;
    u_xlat1.xyz = textureLod(_WindNoiseTexture, u_xlat0.xy, 0.0).xyz;
    u_xlat0.xyz = textureLod(_WindNoiseTexture, u_xlat0.zw, 0.0).xyz;
    u_xlat1.xyz = log2(u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz * vec3(2.5, 2.5, 2.5);
    u_xlat1.xyz = exp2(u_xlat1.xyz);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * in_COLOR0.xyz + in_POSITION0.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD2 = 0.0;
    vs_TEXCOORD3.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD3.zw = vec2(0.0, 0.0);
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = max(u_xlat12, 1.17549435e-38);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat16_2.x = u_xlat0.y * u_xlat0.y;
    u_xlat16_2.x = u_xlat0.x * u_xlat0.x + (-u_xlat16_2.x);
    u_xlat16_1 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_2.xyz = unity_SHC.xyz * u_xlat16_2.xxx + u_xlat16_3.xyz;
    vs_TEXCOORD5.xyz = u_xlat0.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_2.xyz = u_xlat16_2.xyz + u_xlat16_3.xyz;
    u_xlat16_2.xyz = max(u_xlat16_2.xyz, vec3(0.0, 0.0, 0.0));
    vs_TEXCOORD4.xyz = u_xlat16_2.xyz;
    vs_TEXCOORD4.w = 0.0;
    vs_TEXCOORD5.w = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0[4];
uniform 	vec4 _MainLightPosition;
uniform 	mediump vec4 _MainLightColor;
uniform 	mediump vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[16];
uniform 	mediump vec4 _AdditionalLightsColor[16];
uniform 	mediump vec4 _AdditionalLightsAttenuation[16];
uniform 	mediump vec4 _AdditionalLightsSpotDir[16];
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
uniform 	vec4 _CascadeShadowSplitSpheres0;
uniform 	vec4 _CascadeShadowSplitSpheres1;
uniform 	vec4 _CascadeShadowSplitSpheres2;
uniform 	vec4 _CascadeShadowSplitSpheres3;
uniform 	vec4 _CascadeShadowSplitSphereRadii;
uniform 	mediump vec4 _MainLightShadowParams;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_RenderingLayer;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM mediump vec4 unity_SpecCube1_HDR;
	UNITY_UNIFORM vec4 unity_SpecCube0_BoxMax;
	UNITY_UNIFORM vec4 unity_SpecCube0_BoxMin;
	UNITY_UNIFORM vec4 unity_SpecCube0_ProbePosition;
	UNITY_UNIFORM vec4 unity_SpecCube1_BoxMax;
	UNITY_UNIFORM vec4 unity_SpecCube1_BoxMin;
	UNITY_UNIFORM vec4 unity_SpecCube1_ProbePosition;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixPreviousM[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixPreviousMI[4];
	UNITY_UNIFORM vec4 unity_MotionVectorsParams;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _TextureSample_ST;
	UNITY_UNIFORM float _WindJitter;
	UNITY_UNIFORM float _WindScroll;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump sampler2D _TextureSample;
UNITY_LOCATION(1) uniform mediump sampler2D _TextureRamp;
UNITY_LOCATION(2) uniform mediump sampler2D _MainLightShadowmapTexture;
UNITY_LOCATION(3) uniform mediump sampler2DShadow hlslcc_zcmp_MainLightShadowmapTexture;
in highp vec3 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD3;
in highp vec4 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump float u_xlat16_0;
int u_xlati0;
uvec3 u_xlatu0;
bvec4 u_xlatb0;
vec3 u_xlat1;
mediump vec4 u_xlat16_1;
vec3 u_xlat2;
uint u_xlatu2;
vec4 u_xlat3;
mediump vec4 u_xlat16_3;
int u_xlati3;
uint u_xlatu3;
bool u_xlatb3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
vec3 u_xlat7;
mediump vec3 u_xlat16_8;
vec3 u_xlat9;
int u_xlati9;
uint u_xlatu9;
bool u_xlatb9;
vec3 u_xlat12;
int u_xlati12;
float u_xlat14;
float u_xlat18;
int u_xlati18;
uint u_xlatu18;
float u_xlat27;
int u_xlati27;
bool u_xlatb27;
uint u_xlatu29;
mediump float u_xlat16_31;
int int_bitfieldInsert(int base, int insert, int offset, int bits) {
    uint mask = ~(uint(0xffffffffu) << uint(bits)) << uint(offset);
    return int((uint(base) & ~mask) | ((uint(insert) << uint(offset)) & mask));
}

void main()
{
ImmCB_0[0] = vec4(1.0,0.0,0.0,0.0);
ImmCB_0[1] = vec4(0.0,1.0,0.0,0.0);
ImmCB_0[2] = vec4(0.0,0.0,1.0,0.0);
ImmCB_0[3] = vec4(0.0,0.0,0.0,1.0);
vec4 hlslcc_FragCoord = vec4(gl_FragCoord.xyz, 1.0/gl_FragCoord.w);
    u_xlat0.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres0.xyz);
    u_xlat1.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres1.xyz);
    u_xlat2.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres2.xyz);
    u_xlat3.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres3.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.y = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.z = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat0.w = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlatb0 = lessThan(u_xlat0, _CascadeShadowSplitSphereRadii);
    u_xlat16_1.x = (u_xlatb0.x) ? float(1.0) : float(0.0);
    u_xlat16_1.y = (u_xlatb0.y) ? float(1.0) : float(0.0);
    u_xlat16_1.z = (u_xlatb0.z) ? float(1.0) : float(0.0);
    u_xlat16_1.w = (u_xlatb0.w) ? float(1.0) : float(0.0);
    u_xlat16_4.x = (u_xlatb0.x) ? float(-1.0) : float(-0.0);
    u_xlat16_4.y = (u_xlatb0.y) ? float(-1.0) : float(-0.0);
    u_xlat16_4.z = (u_xlatb0.z) ? float(-1.0) : float(-0.0);
    u_xlat16_4.xyz = u_xlat16_1.yzw + u_xlat16_4.xyz;
    u_xlat16_1.yzw = max(u_xlat16_4.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_4.x = dot(u_xlat16_1, vec4(4.0, 3.0, 2.0, 1.0));
    u_xlat16_4.x = (-u_xlat16_4.x) + 4.0;
    u_xlatu0.x = uint(u_xlat16_4.x);
    u_xlati0 = int(int(u_xlatu0.x) << 2);
    u_xlat9.xyz = vs_TEXCOORD0.yyy * hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati0 + 1)].xyz;
    u_xlat9.xyz = hlslcc_mtx4x4_MainLightWorldToShadow[u_xlati0].xyz * vs_TEXCOORD0.xxx + u_xlat9.xyz;
    u_xlat9.xyz = hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati0 + 2)].xyz * vs_TEXCOORD0.zzz + u_xlat9.xyz;
    u_xlat0.xyz = u_xlat9.xyz + hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati0 + 3)].xyz;
    u_xlat16_4.x = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlati27 = int(u_xlat16_4.x);
    u_xlat2.x = float(0.0);
    u_xlat2.y = float(0.0);
    u_xlat2.z = float(0.0);
    for(uint u_xlatu_loop_1 = uint(uint(0u)) ; u_xlatu_loop_1<uint(u_xlati27) ; u_xlatu_loop_1++)
    {
        u_xlatu3 = uint(u_xlatu_loop_1 >> 2u);
        u_xlati12 = int(uint(u_xlatu_loop_1 & 3u));
        u_xlat3.x = dot(unity_LightIndices[int(u_xlatu3)], ImmCB_0[u_xlati12]);
        u_xlati3 = int(u_xlat3.x);
        u_xlat12.xyz = (-vs_TEXCOORD0.xyz) * _AdditionalLightsPosition[u_xlati3].www + _AdditionalLightsPosition[u_xlati3].xyz;
        u_xlat5.x = dot(u_xlat12.xyz, u_xlat12.xyz);
        u_xlat5.x = max(u_xlat5.x, 6.10351563e-05);
        u_xlat14 = inversesqrt(u_xlat5.x);
        u_xlat12.xyz = u_xlat12.xyz * vec3(u_xlat14);
        u_xlat14 = float(1.0) / float(u_xlat5.x);
        u_xlat5.x = u_xlat5.x * _AdditionalLightsAttenuation[u_xlati3].x + _AdditionalLightsAttenuation[u_xlati3].y;
        u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
        u_xlat5.x = u_xlat5.x * u_xlat14;
        u_xlat16_4.x = dot(_AdditionalLightsSpotDir[u_xlati3].xyz, u_xlat12.xyz);
        u_xlat16_4.x = u_xlat16_4.x * _AdditionalLightsAttenuation[u_xlati3].z + _AdditionalLightsAttenuation[u_xlati3].w;
        u_xlat16_4.x = clamp(u_xlat16_4.x, 0.0, 1.0);
        u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
        u_xlat12.x = u_xlat16_4.x * u_xlat5.x;
        u_xlat2.xyz = _AdditionalLightsColor[u_xlati3].xyz * u_xlat12.xxx + u_xlat2.xyz;
    }
    u_xlat3.xy = vs_TEXCOORD3.xy * _TextureSample_ST.xy + _TextureSample_ST.zw;
    u_xlat16_1 = texture(_TextureSample, u_xlat3.xy);
    u_xlat3.xyz = vs_TEXCOORD5.xyz;
    u_xlat3.w = 1.0;
    u_xlat16_4.x = dot(unity_SHAr, u_xlat3);
    u_xlat16_4.y = dot(unity_SHAg, u_xlat3);
    u_xlat16_4.z = dot(unity_SHAb, u_xlat3);
    u_xlat16_3 = vs_TEXCOORD5.yzzx * vs_TEXCOORD5.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_3);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_3);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_3);
    u_xlat16_31 = vs_TEXCOORD5.y * vs_TEXCOORD5.y;
    u_xlat16_31 = vs_TEXCOORD5.x * vs_TEXCOORD5.x + (-u_xlat16_31);
    u_xlat16_6.xyz = unity_SHC.xyz * vec3(u_xlat16_31) + u_xlat16_6.xyz;
    u_xlat16_4.xyz = u_xlat16_4.xyz + u_xlat16_6.xyz;
    u_xlat16_4.xyz = max(u_xlat16_4.xyz, vec3(0.0, 0.0, 0.0));
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat16_0 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec0, 0.0);
    u_xlat16_31 = (-_MainLightShadowParams.x) + 1.0;
    u_xlat16_31 = u_xlat16_0 * _MainLightShadowParams.x + u_xlat16_31;
    u_xlatb0.x = 0.0>=u_xlat0.z;
    u_xlatb9 = u_xlat0.z>=1.0;
    u_xlatb0.x = u_xlatb9 || u_xlatb0.x;
    u_xlat16_31 = (u_xlatb0.x) ? 1.0 : u_xlat16_31;
    u_xlat0.x = u_xlat16_31 * unity_LightData.z;
    u_xlat0.xyz = u_xlat0.xxx * _MainLightColor.xyz;
    u_xlat27 = dot(vs_TEXCOORD5.xyz, _MainLightPosition.xyz);
    u_xlat27 = u_xlat27 * 0.495000005 + 0.5;
    u_xlat5.xyz = u_xlat16_1.xyz * u_xlat16_4.xyz;
    u_xlat5.xyz = u_xlat2.xyz * u_xlat16_1.xyz + u_xlat5.xyz;
    u_xlat7.xyz = u_xlat0.xyz * u_xlat16_1.xyz;
    u_xlat16_8.xyz = texture(_TextureRamp, vec2(u_xlat27)).xyz;
    u_xlat0.x = max(u_xlat0.y, u_xlat0.x);
    u_xlat0.x = max(u_xlat0.z, u_xlat0.x);
    u_xlat9.x = max(u_xlat2.y, u_xlat2.x);
    u_xlat9.x = max(u_xlat2.z, u_xlat9.x);
    u_xlat0.x = u_xlat27 * u_xlat0.x + u_xlat9.x;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat16_8.xyz;
    u_xlat16_1.xyz = u_xlat7.xyz * u_xlat0.xyz + u_xlat5.xyz;
    u_xlat0.x = u_xlat16_1.w + -0.5;
    u_xlatb0.x = u_xlat0.x<0.0;
    if(u_xlatb0.x){discard;}
    u_xlatu0.xyz = uvec3(hlslcc_FragCoord.xyz);
    u_xlatb27 = unity_LODFade.x<0.0;
    u_xlat2.x = unity_LODFade.x + 1.0;
    u_xlat27 = (u_xlatb27) ? u_xlat2.x : unity_LODFade.x;
    u_xlatu18 = u_xlatu0.z * 1025u;
    u_xlatu2 = uint(u_xlatu18 >> 6u);
    u_xlati18 = int(uint(u_xlatu18 ^ u_xlatu2));
    u_xlatu18 = uint(u_xlati18) * 9u;
    u_xlatu2 = uint(u_xlatu18 >> 11u);
    u_xlati18 = int(uint(u_xlatu18 ^ u_xlatu2));
    u_xlati18 = u_xlati18 * 32769;
    u_xlati9 = int(uint(uint(u_xlati18) ^ u_xlatu0.y));
    u_xlatu9 = uint(u_xlati9) * 1025u;
    u_xlatu18 = uint(u_xlatu9 >> 6u);
    u_xlati9 = int(uint(u_xlatu18 ^ u_xlatu9));
    u_xlatu9 = uint(u_xlati9) * 9u;
    u_xlatu18 = uint(u_xlatu9 >> 11u);
    u_xlati9 = int(uint(u_xlatu18 ^ u_xlatu9));
    u_xlati9 = u_xlati9 * 32769;
    u_xlati0 = int(uint(uint(u_xlati9) ^ u_xlatu0.x));
    u_xlatu0.x = uint(u_xlati0) * 1025u;
    u_xlatu9 = uint(u_xlatu0.x >> 6u);
    u_xlati0 = int(uint(u_xlatu9 ^ u_xlatu0.x));
    u_xlatu0.x = uint(u_xlati0) * 9u;
    u_xlatu9 = uint(u_xlatu0.x >> 11u);
    u_xlati0 = int(uint(u_xlatu9 ^ u_xlatu0.x));
    u_xlati0 = u_xlati0 * 32769;
    u_xlat0.x = intBitsToFloat(int(int_bitfieldInsert(1065353216, u_xlati0, 0 & 0x1F, 23)));
    u_xlat0.x = u_xlat0.x + -1.0;
    u_xlatb9 = u_xlat27>=0.5;
    u_xlat18 = (-u_xlat0.x) + 1.0;
    u_xlat0.x = (u_xlatb9) ? u_xlat0.x : u_xlat18;
    u_xlat0.x = (-u_xlat0.x) + u_xlat27;
    u_xlatb0.x = u_xlat0.x<0.0;
    if(u_xlatb0.x){discard;}
    SV_Target0 = u_xlat16_1;
    return;
}

#endif
                             $Globals�        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                    0      _AdditionalLightsColor                   0     _AdditionalLightsAttenuation                 0     _AdditionalLightsSpotDir                 0     _CascadeShadowSplitSpheres0                   p     _CascadeShadowSplitSpheres1                   �     _CascadeShadowSplitSpheres2                   �     _CascadeShadowSplitSpheres3                   �     _CascadeShadowSplitSphereRadii                    �     _MainLightShadowParams                    �     _MainLightWorldToShadow                 0         UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_RenderingLayer                  �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_SpecCube1_HDR                         unity_SpecCube0_BoxMax                         unity_SpecCube0_BoxMin                          unity_SpecCube0_ProbePosition                     0     unity_SpecCube1_BoxMax                    @     unity_SpecCube1_BoxMin                    P     unity_SpecCube1_ProbePosition                     `     unity_LightmapST                  p     unity_DynamicLightmapST                   �  
   unity_SHAr                    �  
   unity_SHAg                    �  
   unity_SHAb                    �  
   unity_SHBr                    �  
   unity_SHBg                    �  
   unity_SHBb                    �  	   unity_SHC                     �     unity_MotionVectorsParams                     �     unity_ObjectToWorld                         unity_WorldToObject                  @      unity_MatrixPreviousM                          unity_MatrixPreviousMI                   @         UnityPerMaterial         _TextureSample_ST                            _WindJitter                         _WindScroll                             $GlobalsP         _TimeParameters                          unity_MatrixVP                                _TextureSample                    _TextureRamp                _MainLightShadowmapTexture                  _WindNoiseTexture                   UnityPerDraw              UnityPerMaterial             m   pow(f, e) will not work for negative f, use abs(f) or conditionally handle negative values if you expect them     Compiling Subshader: 0, Pass: Pass 0, Vertex program with LOD_FADE_CROSSFADE _ADDITIONAL_LIGHTS _MAIN_LIGHT_SHADOWS_CASCADE _MIXED_LIGHTING_SUBTRACTIVE
Platform defines: SHADER_API_GLES30 SHADER_API_MOBILE UNITY_ENABLE_REFLECTION_BUFFERS UNITY_FRAMEBUFFER_FETCH_AVAILABLE UNITY_LIGHTMAP_RGBM_ENCODING UNITY_NO_CUBEMAP_ARRAY UNITY_NO_DXT5nm UNITY_NO_RGBM UNITY_NO_SCREENSPACE_SHADOWS UNITY_PBS_USE_BRDF2
Disabled keywords: DEBUG_DISPLAY DIRLIGHTMAP_COMBINED FOG_EXP FOG_EXP2 FOG_LINEAR INSTANCING_ON LIGHTMAP_ON UNITY_ASTC_NORMALMAP_ENCODING UNITY_COLORSPACE_GAMMA UNITY_ENABLE_DETAIL_NORMALMAP UNITY_ENABLE_NATIVE_SHADOW_LOOKUPS UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS UNITY_HARDWARE_TIER1 UNITY_HARDWARE_TIER2 UNITY_HARDWARE_TIER3 UNITY_LIGHTMAP_DLDR_ENCODING UNITY_LIGHTMAP_FULL_HDR UNITY_LIGHT_PROBE_PROXY_VOLUME UNITY_METAL_SHADOWS_USE_POINT_FILTERING UNITY_NO_FULL_STANDARD_SHADER UNITY_PBS_USE_BRDF1 UNITY_PBS_USE_BRDF3 UNITY_PRETRANSFORM_TO_DISPLAY_ORIENTATION UNITY_SPECCUBE_BLENDING UNITY_SPECCUBE_BOX_PROJECTION UNITY_UNIFIED_SHADER_PRECISION_MODEL UNITY_USE_DITHER_MASK_FOR_ALPHABLENDED_SHADOWS UNITY_VIRTUAL_TEXTURING _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHT_SHADOWS _DBUFFER_MRT1 _DBUFFER_MRT2 _DBUFFER_MRT3 _MAIN_LIGHT_SHADOWS _MAIN_LIGHT_SHADOWS_SCREEN _SAMPLE_GI _SHADOWS_SOFT      	   '     